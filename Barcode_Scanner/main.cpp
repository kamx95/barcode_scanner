#include "opencv2/opencv.hpp"
#include <algorithm>
#include <regex>
#define CORRECT_CAMERA 1

using namespace cv;

void validateAndTrim(std::string &binCode)
{
	std::regex pattern("101[0-1]{42}01010[0-1]{42}101");
	std::smatch result;
	if (std::regex_search(binCode, result, pattern))
	{
		binCode = result[0];
	}	
	else
	{
		binCode.resize(0);
		//std::cout << "Validation Error\n";
	}
}
void calcFirstDigit(std::vector<int> &ean13)
{
	const std::map<int, int> parityMap = { {0,0},{1011,1},{1101,2},{1110,3},
	{10011,4}, {11001,5}, {11100,6}, {10101,7}, {10110,8}, {11010,9} };
	int temp = 0;

	if (ean13.size() == 0)
		return;
	for (int i = 1; i < 7; i++)
	{
		temp *= 10;
		if (ean13[i] > 9) // even group
			temp += 1;
	}

	try
	{
		ean13[0] = parityMap.at(temp);
	}
	catch (const std::out_of_range&)
	{
		std::cout << "Unknown parity pattern\n";
		ean13.resize(0);
		return;
	}
}
void validateChecksum(std::vector<int> &ean13)
{
	int sum = 0;
	if (ean13.size() == 0)
		return;
	for (auto &i : ean13)
	{
		i %= 10;
	}
	for (int i = 0; i < ean13.size() - 1; i++)
	{
		sum += i % 2 == 0 ? ean13[i] : 3 * ean13[i];
	}
	if (ean13[12] != (10 - sum % 10))
	{
		std::cout << "Bad checksum\n";
		ean13.resize(0);
	}
}
void expandDecCode(const std::vector<int> &vecDec, std::string &rawBin)
{
	rawBin.clear();
	for (const auto &x : vecDec)
	{
		if (x < 0)
		{
			std::vector<char> temp(-x, '0');
			rawBin.insert(rawBin.end(), temp.begin(), temp.end());
		}
		else
		{
			std::vector<char> temp(x, '1');
			rawBin.insert(rawBin.end(), temp.begin(), temp.end());
		}
	}
}
void ean13Decode(std::string &binCode, std::vector<int> &ean13, const bool loop_guard=true)
{
	const int guard = 3;
	const int mid_guard = 5;
	int temp = 0;
	int i = 1;

	if (binCode.size() == 0)
	{
		ean13.resize(0);
		return;
	}

	// 3 groups: odd (A) [0-9], even (B) [10-19], even on the right (C) [20-29]
	const std::map<int, int> decodeMap = { {1101, 0}, {11001, 1}, {10011, 2},
	{ 111101 ,3}, { 100011 ,4}, { 110001,5}, {101111, 6}, {111011, 7},
	{ 110111 ,8}, { 1011 ,9}, { 100111, 10 }, { 110011,11 }, { 11011 ,12 },
	{ 100001 ,13 }, { 11101 ,14 }, { 111001,15 }, { 101, 16 }, { 10001, 17 },
	{ 1001 ,18 }, { 10111 ,19 }, { 1110010, 20 }, { 1100110, 21 }, { 1101100, 22 },
	{ 1000010 ,23 }, { 1011100 ,24 }, { 1001110, 25 }, { 1010000, 26 }, { 1000100, 27 },
	{ 1001000 ,28 }, { 1110100 ,29 } };

	ean13.resize(13);
	for (auto it = binCode.begin() + guard; it < binCode.begin() + 6 * 7 + guard; it++, i++) // left side
	{
		temp *= 10;
		temp += *it - '0';
		if (i % 7 == 0)
		{
			try
			{
				ean13[i / 7] = decodeMap.at(temp);
			}
			catch (const std::out_of_range&)
			{
				std::cout << "Can't decode left side (unknown pattern)\n";
				ean13.resize(0);
				return;
			}
			temp = 0;

			if (i == 7)
			{
				if (ean13[i / 7] > 9) //first digit encoded with B or C; upside down code detection
				{
					if (!loop_guard)
					{
						std::cout << "Both sides encoded with the same parity group\n";
						ean13.resize(0);
						return;
					}
					std::reverse(binCode.begin(), binCode.end());
					ean13Decode(binCode, ean13, false);
					return;
				}
			}
		}
	}

	temp = 0;
	for (auto it = binCode.begin() + 6 * 7 + mid_guard + guard; it < binCode.end(); it++, i++) //right side
	{
		temp *= 10;
		temp += *it -'0';
		if (i % 7 == 0)
		{
			try
			{
				ean13[i / 7] = decodeMap.at(temp);
			}
			catch (const std::out_of_range&)
			{
				std::cout << "Can't decode right side (unknown pattern)\n";
				ean13.resize(0);
				return;
			}
			temp = 0;

			if (ean13[i / 7] < 20) //rigth side, only C group
			{
				std::cout << "Can't decode right side (wrong parity group)\n";
				ean13.resize(0);
				return;
			}
		}
	}
}
void getRawLine(const Mat &threshImage, const int y, std::vector<std::vector<int> > &rawLines)
{
	if (threshImage.at<uchar>(Point(0,y)) > 254)
		rawLines.push_back(std::vector<int>(1, -1)); //white
	else
		rawLines.push_back(std::vector<int>(1, 1)); //black

	std::vector<int> &vec_ref = rawLines.back();

	for (int i = 1; i < threshImage.cols; i++)
	{
		if (threshImage.at<uchar>(Point(i,y)) > 254)
		{
			if (vec_ref.back() < 0)
				vec_ref.back()--;
			else
				vec_ref.push_back(-1);
		}
		else
		{
			if (vec_ref.back() > 0)
				vec_ref.back()++;
			else
				vec_ref.push_back(1);
		}
	}
}
void getImageFromRotatedRect(Mat &inputOutputImage, const RotatedRect &rot_rect)
{
	float angle = rot_rect.angle;
	Size r_size = rot_rect.size;
	Mat rot_matrix, rotated;
	Rect bound = rot_rect.boundingRect();
	if ((bound & Rect(Point(0, 0), inputOutputImage.size())) != bound) // Check if rect is inside image
	{
		inputOutputImage.release();
		return;
	}
	Mat input_ROI = inputOutputImage(bound);
	Point2f im_center(input_ROI.cols / 2.f, input_ROI.rows / 2.f);
	if (angle < -45.0)
	{
		angle += 90;
		swap(r_size.width, r_size.height);
	}
	rot_matrix = getRotationMatrix2D(im_center, angle, 1.0);
	warpAffine(input_ROI, rotated, rot_matrix, input_ROI.size(), INTER_CUBIC);
	rotated.copyTo(inputOutputImage);
	getRectSubPix(inputOutputImage, r_size, im_center, inputOutputImage);
	if (bound.height > bound.width)
		rotate(inputOutputImage, inputOutputImage, ROTATE_90_CLOCKWISE);
}
void processBarcode(const Mat &image, std::vector<int> &outCode)
{
	std::vector<std::vector<int> > rawLines;
	Mat threshImage;

	threshold(image, threshImage, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
	imshow("Thresh", threshImage);
	
	for (int i = 0; i < threshImage.rows; i += threshImage.rows / 12 + 1)
	{
		getRawLine(threshImage, i, rawLines);
	}

	for (const auto &raw : rawLines)
	{
		std::set<int> rawSorted;
		std::set<int>::iterator setIt;
		std::string binCode;

		for (const int &i : raw) { rawSorted.insert(abs(i)); }
		int i = 0;
		setIt = rawSorted.begin();
		//finding scale coefficient
		do
		{
			std::vector<int> tempVec;
			tempVec.reserve(raw.size());
			for (const int &j : raw)
				tempVec.push_back(j / (*setIt));
			expandDecCode(tempVec, binCode);
			validateAndTrim(binCode);
			setIt++;
			i++;
		} while (binCode.size() == 0 && setIt != rawSorted.end() && i < 5);

		ean13Decode(binCode, outCode);
		calcFirstDigit(outCode);
		validateChecksum(outCode);

		if (outCode.size() != 0)
			break;
	}
}
//Gradient based barcode detection
int process_image(Mat &io_image)
{
	Mat grey_image, gradX, gradY, gradVer, gradHor;
	Mat gradYbin, gradXbin;
	cvtColor(io_image, grey_image, CV_BGR2GRAY);
	Scharr(grey_image, gradX, CV_32F, 1, 0);
	Scharr(grey_image, gradY, CV_32F, 0, 1);
	
	convertScaleAbs(gradX, gradX);
	convertScaleAbs(gradY, gradY);

	threshold(gradY, gradYbin, 180, 255, CV_THRESH_BINARY_INV);
	threshold(gradX, gradXbin, 180, 255, CV_THRESH_BINARY_INV);

	gradX.copyTo(gradVer, gradYbin); //Get gradX where gradY ~ 0
	gradY.copyTo(gradHor, gradXbin); //Get gradY where gradX ~ 0

	threshold(gradVer, gradVer, 230, 255, CV_THRESH_BINARY);
	threshold(gradHor, gradHor, 230, 255, CV_THRESH_BINARY);

	Mat kernel = getStructuringElement(CV_SHAPE_RECT, Size(3, 3));
	morphologyEx(gradVer, gradVer, CV_MOP_CLOSE, kernel,Point(-1,-1),3);
	morphologyEx(gradVer, gradVer, CV_MOP_OPEN, kernel);

	morphologyEx(gradHor, gradHor, CV_MOP_CLOSE, kernel, Point(-1, -1), 3);
	morphologyEx(gradHor, gradHor, CV_MOP_OPEN, kernel);

	std::vector<std::vector<Point> > contours, cntHor;
	findContours(gradVer.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
	findContours(gradHor.clone(), cntHor, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

	bool flag_hor = 1;
	double max = 0;
	if (contours.size() == 0)
		return -1;
	auto max_it = contours.begin();

	for (auto it = max_it; it<contours.end(); it++)
	{
		if (contourArea(*it) > max)
		{
			max = contourArea(*it);
			max_it = it;
		}
	}

	for (auto it = cntHor.begin(); it<cntHor.end(); it++)
	{
		if (contourArea(*it) > max)
		{
			max = contourArea(*it);
			max_it = it;
			flag_hor = 0;
		}
	}

	RotatedRect rot_box = minAreaRect(*max_it);

	Rect bound_box = rot_box.boundingRect();
	if ((bound_box.width < bound_box.height) == flag_hor)
	{
		return -1;
	}

	Point2f verticles[4];
	rot_box.points(verticles);
	for (int i=0; i<4; i++)
	{
		line(io_image, verticles[i], verticles[(i + 1) % 4], Scalar(255, 0, 0), 3);
	}

	std::vector<int> code;
	getImageFromRotatedRect(grey_image, rot_box);
	if (grey_image.data == NULL)
		return -1;
	processBarcode(grey_image, code);
	if (code.size())
	{
		std::cout << "\nI've got this!\n";
		for (auto const &i : code)
			std::cout << i;
		std::cout << "\n\n";
		for (int i = 0; i<4; i++)
		{
			line(io_image, verticles[i], verticles[(i + 1) % 4], Scalar(0, 255, 0), 3);
		}
		return 0;
	}
	return -1;
}
//Hough transform barcode detection
int process_image_hough(Mat &io_image)
{
	const int MIN_AREA = 2500;
	const float MIN_FILL_RATIO = 0.6f;
	Mat cannyTemp, contour_image(io_image.size(), CV_8U), grey_image;
	Mat kernel = getStructuringElement(CV_SHAPE_RECT, Size(3, 3));
	std::vector<Vec4i> lines;
	std::vector<int> code;
	std::vector<std::vector<Point> > contours;

	cvtColor(io_image, grey_image, CV_BGR2GRAY);
	Canny(grey_image, cannyTemp, 100, 200);
	contour_image = 0;
	
	HoughLinesP(cannyTemp, lines, 1, CV_PI / 180, 10,0,10);

	for (const auto &l:lines)
		line(contour_image, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255), 1, LineTypes::LINE_4);

	morphologyEx(contour_image, contour_image, CV_MOP_CLOSE, kernel);
	morphologyEx(contour_image, contour_image, CV_MOP_OPEN, kernel, Point(-1,-1), 2);
	morphologyEx(contour_image, contour_image, CV_MOP_CLOSE, kernel, Point(-1, -1), 5);

	findContours(contour_image.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

	double max = 0;
	if (contours.size() == 0)
		return -1;
	auto max_it = contours.begin();
	for (auto it = max_it; it<contours.end(); it++)
	{
		if (contourArea(*it) > max)
		{
			max = contourArea(*it);
			max_it = it;
		}
	}

	RotatedRect rot_box = minAreaRect(*max_it);
	Rect bound_box = rot_box.boundingRect();
	
	if (rot_box.size.area() < MIN_AREA)
		return -1;
	getImageFromRotatedRect(grey_image, rot_box);
	getImageFromRotatedRect(contour_image, rot_box);

	if (grey_image.data == NULL)
		return -1;
	if (countNonZero(contour_image) < contour_image.cols*contour_image.rows * MIN_FILL_RATIO)
		return -1;

	Point2f verticles[4];
	rot_box.points(verticles);
	for (int i = 0; i<4; i++)
		line(io_image, verticles[i], verticles[(i + 1) % 4], Scalar(255, 0, 0), 3);

	processBarcode(grey_image, code);
	if (code.size())
	{
		std::cout << "\nI've got this!\n";
		for (auto const &i : code)
			std::cout << i;
		std::cout << "\n\n";
		for (int i = 0; i<4; i++)
		{
			line(io_image, verticles[i], verticles[(i + 1) % 4], Scalar(0, 255, 0), 3);
		}
		return 0;
	}
	return -1;
	
}
int main()
{
	bool correction_data_loaded = 0;
	Mat map1, map2; //correction maps
	Mat corrected_frame;

	if (CORRECT_CAMERA)
	{
		FileStorage correction_data("out_camera_data.xml", FileStorage::READ);
		if (!correction_data.isOpened())
		{
			std::cout << "Can't open corretion data file. (out_camera_data.xml)\n";
			std::cout << "Moving on with no input correction\n";
		}
		else
		{
			Mat cameraMatrix, distCoeffs;
			int cols = 0, rows = 0;
			correction_data["camera_matrix"] >> cameraMatrix;
			correction_data["distortion_coefficients"] >> distCoeffs;
			correction_data["image_width"] >> cols;
			correction_data["image_height"] >> rows;
			if (cameraMatrix.data != NULL && distCoeffs.data != NULL && cols*rows!=0)
			{
				correction_data_loaded = 1;
				initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(), cameraMatrix, Size(cols, rows), CV_32FC1, map1, map2);
				std::cout << "Correction data loaded\n";
			}
		}
		correction_data.release();
	}

	VideoCapture cap(0);
	if (!cap.isOpened())
		return -1;
		
	/*
	// No camera testing
	Mat image = imread("cap.png", CV_LOAD_IMAGE_COLOR);
	process_image_hough(image);
	imshow("LoadedImg", image);
	waitKey(0);
	return 0;
	*/

	namedWindow("Camera", 1);
	for (;;)
	{
		Mat frame;
		cap >> frame;
		char key = waitKey(30);
		if (correction_data_loaded)
		{
			remap(frame, corrected_frame, map1, map2, CV_INTER_CUBIC);
			if (key == 's')
			{
				imwrite("cap.png", corrected_frame);
				break;
			}			
			//save frame for debug
			imwrite("debug.png", corrected_frame);
			if	(process_image_hough(corrected_frame)==0)
			{
				imshow("Camera", corrected_frame);
				waitKey(0);
			}	
			imshow("Camera", corrected_frame);
		}
		else
		{
			if (key == 's')
			{
				imwrite("cap.png", frame);
				break;
			}
			//save frame for debug
			imwrite("debug.png", frame);
			if (process_image_hough(frame) == 0)
			{
				imshow("Camera", frame);
				waitKey(0);
			}
			imshow("Camera", frame);
		}
		if (key =='q') break;
	}
	destroyAllWindows();
	
	return 0;
}